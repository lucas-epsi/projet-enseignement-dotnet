
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class People
{

    private String lastName;

    private String firstName;

    private DateTime birthdate;

    public People(String firstName, String LastName, DateTime birthdate)
    {
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthdate = birthdate;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getFirstName()
    {
        return this.firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public DateTime getBirthdate()
    {
        return this.birthdate;
    }

    public void setBirthdate(DateTime birthdate)
    {
        this.birthdate = birthdate;
    }

    public int getAge()
    {

        var today = DateTime.Today;
        var age = today.Year - birthdate.Year;
        if (birthdate.Date > today.AddYears(-age)) age--;

        return age;
    }

}