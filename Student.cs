
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Student : People {

    private Study study;

    public Study getStudy()
    {
        return this.study;
    }

    public void setStudy(Study study)
    {
        this.study = study;
    }

    public Student(String name, String firstName, DateTime birthdate) : base(name, firstName, birthdate) 
    {
        
    }

    public List<Note> getHisNotes() {
        return new List<Note>();
    }

    public void getMean(){
        List<Note> notes = getHisNotes();
        int acc = 0;
        int total = 0;
        foreach(Note item in notes){
            acc += item.getValue();
            
        }
    }

}