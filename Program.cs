﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Model
{
    class Program
    {
        static void Main(string[] args)
        {
            //init

            Student student1 = new Student("toto", "rina", new DateTime(1992, 11, 30));
            Student student2 = new Student("don", "corleone", new DateTime(1993, 11, 30));
            Teacher jplepilier = new Teacher("Jean Paul", "Damestoy", new DateTime(1950, 6, 9));
            Study epsi_b3 = new Study("epsi_b3");
            Subject cours_test = new Subject("Unit Tests", 50, 100, "vive les tests", jplepilier);
            Eval eval1 = new Eval("test binouze");

            epsi_b3.addStudent(student1);
            epsi_b3.addStudent(student2);
            eval1.addNote(new Note("Yoan FILIPE", 0, "ne tiens pas l'alcool au dela d'une girafe de triple K"));
            cours_test.addEval(eval1);

            epsi_b3.addSubject(cours_test);

            //display
            foreach (Student item in epsi_b3.getStudents())
            {
                Console.WriteLine(item.getAge());
            }

            foreach (Subject item in epsi_b3.getSubjects())
            {
                Console.WriteLine(item.getName());
            }

            Console.WriteLine(new List<Subject>(student1.getStudy().getSubjects())[0].getEvaluations());

        }

    }
}
