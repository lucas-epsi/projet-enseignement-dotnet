
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Subject
{

    private String name;
    private int hours;
    private int coeff;
    private String content;

    public Subject(String name, int hours, int coeff, String content)
    {
        this.name = name;
        this.hours = hours;
        this.coeff = coeff;
        this.content = content;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getHours()
    {
        return this.hours;
    }

    public void setHours(int hours)
    {
        this.hours = hours;
    }

    public int getCoeff()
    {
        return this.coeff;
    }

    public void setCoeff(int coeff)
    {
        this.coeff = coeff;
    }

    public String getContent()
    {
        return this.content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

}